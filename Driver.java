import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class Driver {


    public static void main(String[] args) {
        try {
            Simulator s = new Simulator();

            DriverManager.registerDriver(s);
            String url = "jdbc:mysql://localhost/portfolio";
            Connection connection = DriverManager.getConnection(url, "root", "GitGudm9!");

            s.setConnection(connection);
            Statement statement = connection.createStatement();
            s.setStatement(statement);

            Date startDate = createDate(2016, 10, 6);
            Date endDate = createDate(2017, 10, 3);
            List<String> stocks = Arrays.asList("goog", "celg", "nvda", "fb");
            List<Float> distributions = Arrays.asList((float) 0.3, (float) 0.3, (float) 0.2, (float) 0.2);

            s.runSim(startDate, endDate, stocks, distributions);
            s.runAllSims(startDate, endDate, stocks);

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
    }

    private static Date createDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set( cal.YEAR, year );
        cal.set( cal.MONTH, month - 1);
        cal.set( cal.DATE,  day);

        return new Date(cal.getTime().getTime());
    }
}
