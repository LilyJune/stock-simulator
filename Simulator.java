import com.mysql.jdbc.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Simulator extends Driver {

    Connection connection;
    Statement statement;

    public Simulator() throws SQLException {

    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public List<Float> runSim(Date startDate, Date endDate, List<String> stocks, List<Float> allocation) throws SQLException {
        //ResultSet rs = statement.executeQuery("SELECT * FROM portfolio");
        float stddev = 0;
        float avgret = 0;
        float sharpe = 0;
        float cumretport =0;

        PreparedStatement ps;

        ps = connection.prepareStatement("UPDATE portfolio.portfolio SET portfolio_cumulative_return = (" + stocks.get(0) + "_cumulative_return* ?) + (" + stocks.get(1) + "_cumulative_return * ?) + (" + stocks.get(2) + "_cumulative_return  * ?) + ("  + stocks.get(3) + "_cumulative_return * ?);");
        ps.setFloat(1, allocation.get(0));
        ps.setFloat(2, allocation.get(1));
        ps.setFloat(3, allocation.get(2));
        ps.setFloat(4, allocation.get(3));

        //System.out.println("value of ps "+ps);
        ps.execute();

        for (int i = 0; i < stocks.size(); i++) {
            ps = connection.prepareStatement("UPDATE portfolio.portfolio SET " + stocks.get(i) + "_value = (" + allocation.get(i) + " * " + stocks.get(i) + "_cumulative_return);");
            //System.out.println("value of ps "+ps);
            ps.execute();
        }

        ps = connection.prepareStatement("UPDATE portfolio.portfolio SET portfolio_value = " + stocks.get(0) + "_value + " + stocks.get(1) + "_value + " + stocks.get(2) + "_value + " + stocks.get(3) + "_value;");
        //System.out.println("value of ps "+ps);
        ps.execute();

        ps = connection.prepareStatement("SELECT count(*) FROM portfolio.portfolio p WHERE p.date BETWEEN ? AND ?;");
        ps.setDate(1, startDate);
        ps.setDate(2, endDate);
        ResultSet rs = ps.executeQuery();

        int count = 0;
        while(rs.next()) {
            count = rs.getInt(1);
        }

        ps = connection.prepareStatement("SELECT spy_cumulative_return, portfolio_cumulative_return FROM portfolio.portfolio p WHERE p.date BETWEEN ? AND ?;");
        ps.setDate(1, startDate);
        ps.setDate(2, endDate);
        rs = ps.executeQuery();

        ArrayList<Float> pms = new ArrayList<>();
        ArrayList<Float> portfolio = new ArrayList<>();

        while(rs.next()) {
            float spy = rs.getFloat(1);
            float port = rs.getFloat(2);

            pms.add(port - spy);
            portfolio.add(port);
        }

        stddev = sd(portfolio);
        avgret = mean(portfolio);
        sharpe = sharpe(count, pms);
        cumretport = (portfolio.get(portfolio.size() - 1) - portfolio.get(0)) / portfolio.get(0);

        System.out.println(stocks);
        System.out.println(allocation);
        System.out.println("Standard Deviation: " + stddev);
        System.out.println("Average Return: " + avgret);
        System.out.println("Sharpe Ratio: " + sharpe);
        System.out.println("Cumulative Return: " + cumretport + "\r\n");

        return Arrays.asList(stddev, avgret, sharpe, cumretport);
    }

    private static float sharpe(int count, ArrayList<Float> pms) {
        float sqrt = (float) Math.sqrt(count);
        float stddev = sd(pms);
        float mean = mean(pms);

        return sqrt * mean / stddev;
    }

    public List runAllSims(Date startDate, Date endDate, List<String> stocks) throws SQLException {
        float bestSharpe = 0;
        List<Float> bestResults = new ArrayList<>();
        List<Float> bestWeights = new ArrayList<>();
        List<List<Float>> possibilities = InttoFloat(getPermutations(10, 4));


        for (List<Float> l: possibilities) {
            List<Float> results = runSim(startDate, endDate, stocks, l);
            if (results.get(2) > bestSharpe) {
                bestSharpe = results.get(2);
                bestWeights = l;
                bestResults = results;
            }

        }
        System.out.println("\r\n####################");
        System.out.println("##      BEST      ##");
        System.out.println("####################\r\n");

        runSim(startDate, endDate, stocks, bestWeights);
        List<List<Float>> retVal = new ArrayList<>();
        retVal.add(bestWeights);
        retVal.add(bestResults);
        return retVal;
    }

    private static float sd (ArrayList<Float> table) {
        double mean = mean(table);
        double temp = 0;
        for (int i = 0; i < table.size(); i++) {
            float val = table.get(i);
            double squrDiffToMean = Math.pow(val - mean, 2);
            temp += squrDiffToMean;
        }
        double meanOfDiffs = temp / (float) (table.size());
        return (float) Math.sqrt(meanOfDiffs);
    }

    private static float mean(ArrayList<Float> table) {
        float total = (float) 0.0;
        for (Float f: table) {
            total += f;
        }

        return total / (float) table.size();
    }

    private List<List<Float>> InttoFloat(List<List<Integer>> intList) {
        List<List<Float>> total = new ArrayList<>();
        for (List<Integer> l: intList) {
            List<Float> temp = new ArrayList<>();
            for (int i: l) {
                temp.add((float)i/(float)10.0);
            }
            total.add(temp);
        }
        return total;
    }

    private List<List<Integer>> getPermutations(final int count, final int groups) {
        if (groups == 1) {
            final List<Integer> inner = new ArrayList<>(1);
            inner.add(count);
            final List<List<Integer>> outer = new ArrayList<>(1);
            outer.add(inner);
            return outer;
        }
        return IntStream.range(0, count + 1)
                .boxed()
                .flatMap(p -> getPermutations(count - p, groups - 1)
                        .stream()
                        .peek(q -> q.add(p)))
                .collect(Collectors.toList());
    }
}
